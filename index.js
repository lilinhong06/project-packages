/**
 * @param {string} hex example: #000000
 * @return {string}
 */
function hex2rgb(hex) {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
    if (!result) return null
    const [, r, g, b] = result
    return `rbg(${parseInt(r, 16)}, ${parseInt(g, 16)}, ${parseInt(b, 16)})`
  }
  
  module.exports = hex2rgb
  